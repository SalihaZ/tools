---
install_repository_dependencies: true
install_resolver_dependencies: true
install_tool_dependencies: false
tools:
  ## get data metabolomics
  - name: mtblsdwnld
    owner: prog
    tool_panel_section_id: metabolomics_getext
  - name: downloader_bank_hmdb
    owner: fgiacomoni
    tool_panel_section_id: metabolomics_getext

  # label_preprocessing
  ## LCMS
  - name: msnbase_readmsdata
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xcms_export_samplemetadata
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xcms_xcmsset
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xcms_plot_chromatogram
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xcms_merge
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xcms_group
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xcms_retcor
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xcms_fillpeaks
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: camera_annotate
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xcms_summary
    owner: lecorguille
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: camera_combinexsannos
    owner: workflow4metabolomics
    tool_panel_section_id: metabolomics_preprocessing_lcms
  - name: xseekerpreparator
    owner: lain
    tool_panel_section_id: metabolomics_preprocessing_lcms

  ## FIAMS
  - name: profia
    owner: ethevenot
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_process_scans
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_replicate_filter
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_align_samples
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_blank_filter
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_sample_filter
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_missing_values_sample_filter
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_merge_peaklists
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_get_peaklists
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams
  - name: dimspy_hdf5_to_txt
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_preprocessing_fiams


  ## GCMS
  - name: metams_rungc
    owner: yguitton
    tool_panel_section_id: metabolomics_preprocessing_gcms
  - name: ramclustr
    owner: recetox
    tool_panel_section_id: metabolomics_preprocessing_gcms


  ## NMR
  - name: nmr_preprocessing
    owner: marie-tremblay-metatoul
    tool_panel_section_id: metabolomics_preprocessing_nmr
  - name: nmr_alignment
    owner: marie-tremblay-metatoul
    tool_panel_section_id: metabolomics_preprocessing_nmr
  - name: nmr_bucketing
    owner: marie-tremblay-metatoul
    tool_panel_section_id: metabolomics_preprocessing_nmr

  # label_quality_processing_ms
  ## LCMS_FIAMS
  - name: batchcorrection
    owner: melpetera
    tool_panel_section_id: metabolomics_quality_processing_ms
  - name: acorf
    owner: melpetera
    tool_panel_section_id: metabolomics_quality_processing_ms
  - name: correlation_analysis
    owner: workflow4metabolomics
    tool_panel_section_id: metabolomics_quality_processing_ms

  ## ALL
  - name: normalization
    owner: marie-tremblay-metatoul
    tool_panel_section_id: metabolomics_quality_processing_all
  - name: transformation
    owner: ethevenot
    tool_panel_section_id: metabolomics_quality_processing_all
  - name: qualitymetrics
    owner: ethevenot
    tool_panel_section_id: metabolomics_quality_processing_all
  - name: withinvariation
    owner: yguitton
    tool_panel_section_id: metabolomics_quality_processing_all
  - name: intensity_checks
    owner: melpetera
    tool_panel_section_id: metabolomics_quality_processing_all

  # label_statistics
  ## ALL
  - name: univariate
    owner: ethevenot
    tool_panel_section_id: metabolomics_statistics_all
  - name: multivariate
    owner: ethevenot
    tool_panel_section_id: metabolomics_statistics_all
  - name: anova
    owner: lecorguille
    tool_panel_section_id: metabolomics_statistics_all
  - name: mixmodel4repeated_measures
    owner: workflow4metabolomics
    tool_panel_section_id: metabolomics_statistics_all
  - name: asca
    owner: marie-tremblay-metatoul
    tool_panel_section_id: metabolomics_statistics_all
  - name: biosigner
    owner: ethevenot
    tool_panel_section_id: metabolomics_statistics_all
  - name: corr_table
    owner: melpetera
    tool_panel_section_id: metabolomics_statistics_all
  - name: heatmap
    owner: ethevenot
    tool_panel_section_id: metabolomics_statistics_all

  # label_annotation
  ## LCMS_FIAMS
  - name: hmdb_ms_search
    owner: fgiacomoni
    tool_panel_section_id: metabolomics_annotation_lcms_fiams
  - name: bank_inhouse
    owner: fgiacomoni
    tool_panel_section_id: metabolomics_annotation_lcms_fiams
  - name: lipidmaps_textsearch
    owner: fgiacomoni
    tool_panel_section_id: metabolomics_annotation_lcms_fiams
  - name: massbank_ws_searchspectrum
    owner: fgiacomoni
    tool_panel_section_id: metabolomics_annotation_lcms_fiams
  - name: bih4maconda
    owner: fgiacomoni
    tool_panel_section_id: metabolomics_annotation_lcms_fiams
  - name: hr2
    owner: fgiacomoni
    tool_panel_section_id: metabolomics_annotation_lcms_fiams
  - name: lcmsmatching
    owner: prog
    tool_panel_section_id: metabolomics_annotation_lcms_fiams

  ## LCMS
  #  - name: probmetab
  #    owner: mmonsoor
  #    tool_panel_section_id: metabolomics_annotation_lcms

  ## GCMS
  - name: golm_ws_lib_search
    owner: fgiacomoni
    tool_panel_section_id: metabolomics_annotation_gcms
  - name: riassigner
    owner: recetox
    tool_panel_section_id: metabolomics_annotation_gcms
  - name: recetox_msfinder
    owner: recetox
    tool_panel_section_id: metabolomics_annotation_gcms
  - name: matchms
    owner: recetox
    tool_panel_section_id: metabolomics_annotation_gcms
  - name: matchms_filtering
    owner: recetox
    tool_panel_section_id: metabolomics_annotation_gcms
  - name: matchms_formatter
    owner: recetox
    tool_panel_section_id: metabolomics_annotation_gcms
  - name: matchms_networking
    owner: recetox
    tool_panel_section_id: metabolomics_annotation_gcms


  ## NMR
  - name: nmr_annotation
    owner: marie-tremblay-metatoul
    tool_panel_section_id: metabolomics_annotation_nmr
  - name: 2dnmrannotation
    owner: marie-tremblay-metatoul
    tool_panel_section_id: metabolomics_annotation_nmr

  # label_data_handling
  ## ALL
  - name: msconvert
    owner: galaxyp
    tool_panel_section_id: metabolomics_data_handling_all
  - name: generic_filter
    owner: melpetera
    tool_panel_section_id: metabolomics_data_handling_all
  - name: tablemerge
    owner: melpetera
    tool_panel_section_id: metabolomics_data_handling_all
  - name: checkformat
    owner: ethevenot
    tool_panel_section_id: metabolomics_data_handling_all
  - name: idchoice
    owner: melpetera
    tool_panel_section_id: metabolomics_data_handling_all
  - name: isa2w4m
    owner: prog
    tool_panel_section_id: metabolomics_data_handling_all
  - name: isaextractor
    owner: prog
    tool_panel_section_id: metabolomics_data_handling_all

  ## MSMS-MSPurity
  - name: mspurity_puritya
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_frag4feature
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_filterfragspectra
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_averagefragspectra
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_createdatabase
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_spectralmatching
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_createmsp
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: metfrag
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: sirius_csifingerid
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_combineannotations
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_flagremove
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_purityx
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name: mspurity_dimspredictpuritysingle
    owner: computational-metabolomics
    tool_panel_section_id: metabolomics_msms
  - name:  ms2snoop
    owner: workflow4metabolomics
    tool_panel_section_id: metabolomics_msms

  ## Fluxomic
  - name: influx_si
    owner: workflow4metabolomics
    tool_panel_section_id: metabolomics_fluxomics_all
  - name: isocor
    owner: gmat
    tool_panel_section_id: metabolomics_fluxomics_all
  - name: isoplot
    owner: workflow4metabolomics
    tool_panel_section_id: metabolomics_fluxomics_all
  - name: physiofit
    owner: workflow4metabolomics
    tool_panel_section_id: metabolomics_fluxomics_all
  - name: physiofit_manager
    owner: workflow4metabolomics
    tool_panel_section_id: metabolomics_fluxomics_all
